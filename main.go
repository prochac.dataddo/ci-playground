package main

import (
	"fmt"

	"gitlab.com/prochac.dataddo/ci-playground/bar"
	"gitlab.com/prochac.dataddo/ci-playground/foo"
)

func Foo() string { return "foo" }
func Bar() string { return "bar" }

func main() {
	fmt.Println(Foo())
	fmt.Println(foo.String())
	fmt.Println(Bar())
	fmt.Println(bar.String())
}
