package main

import "testing"

func TestFoo(t *testing.T) {
	tests := []string{
		"foo",
	}
	for _, test := range tests {
		t.Run(test, func(t *testing.T) {
			if got := Foo(); got != test {
				t.Errorf("Foo() = %v, want %v", got, test)
			}
		})
	}
}
