package bar

import "testing"

func TestString(t *testing.T) {
	tests := []string{
		"bar",
	}
	for _, test := range tests {
		t.Run(test, func(t *testing.T) {
			if got := String(); got != test {
				t.Errorf("String() = %v, want %v", got, test)
			}
		})
	}
}
